var express = require('express');
var nodemailer = require('nodemailer');
var multer  = require('multer')

var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, 'profile/')
	},
	filename: function (req, file, cb) {
		cb(null, file.fieldname + '-' + Date.now()+'.jpg')
	}
})

var upload = multer({ storage: storage })
var router = express.Router();
var ObjectId = require('mongodb').ObjectId;


/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index');
});


/* POST login user. */
/*http://localhost:3000/login*/
router.post('/login',function(req, res){
	var db = req.db;
	var collection = db.get('usercollection');
	var email = req.body.email;
	var password = req.body.password;
	if(email == 'adminsurfin@yopmail.com' && password == '123456'){
		collection.find({},function(e, docs){
			if(e){
				res.send(e);
			}
			else{
				res.send(docs);
			}
		})
	}
	else{
		collection.find({"email" : email, "password" : password},function(e,docs){
			if(docs.length != 0){
				docs[0].status = 1;
				docs[0].message = "login successfully"
				res.send(docs[0]);  
			}
			else{
				var doc = {}
				doc.status = 0;
				doc.message = "Wrong Email id or Password" 
				res.send(doc);
			}
		});  
	}
});

router.get('/getusers', function(req, res, next){
	var db = req.db;
	var collection = db.get('usercollection');
	collection.find({},function(e, docs){
		if(e){
			res.send(e);
		}
		else{
			res.send(docs);
		}
	})
})

router.get('/getteachers', function(req, res){
	var db = req.db;
	var collection = db.get('usercollection');
	collection.find({'type' : 'Teacher'},function(e, docs){
		if(e){
			res.send(e);
		}
		else{
			res.send(docs);
		}
	})
})

/* POST forget password user. */
/*http://localhost:3000/getpassword*/
router.post('/getpassword', function(req, res, next){
	var db = req.db;
	var collection = db.get('usercollection');
	var email = req.body.email;
	collection.find({"email" : email},function(e,docs){
		if(docs.length != 0){
			// docs[0].status = 1;
			// res.send(docs[0]);
			var transporter = nodemailer.createTransport({
				service: 'Gmail',
				auth: {
					user: 'santosh.infograins@gmail.com', // Your email id
					pass: 'infoanoopa27' // Your password
				}
			})
			
			var text = 'Hello '+ req.body.email+', Your Password is' + docs[0].password;
			
			var mailOptions = {
				from: 'santosh.infograins@gmail.com', // sender address
				to:  req.body.email, // list of receivers
				subject: 'Password Recovery', // Subject line
				text: text 
			}

			transporter.sendMail(mailOptions, function(error, info){
				if(error){
					console.log(error)
					res.send(error);
				}else{
					console.log(info)
					res.send(info);
				}
			})  
		}
		else{
			var doc = {}
			doc.status = 0;
			doc.message = "No such email found" 
			res.send(doc);
		}
	});
})

// router.post('/updatenewpassword', function(req, res){
// 	var db = req.db;
// 	var collection = db.get('usercollection');
// 	var id = req.body.data.id;
// 	var password = req.body.data.password;
// 	console.log(req.body)
// 	// collection.update({_id: ObjectId(id)},{$set : {password : password}},function(e, docs){
// 	// 	if(e){
// 	// 		res.send(e)
// 	// 	}
// 	// 	else{
// 	// 		res.send(docs)
// 	// 	}
// 	// })
// })

router.post('/updatepassword', function(req, res, next){
	var db = req.db;
	var collection = db.get('usercollection');
	var id = req.body.id;
	var password = req.body.password;
	collection.update({_id: ObjectId(id)},{$set : {password : password}},function(e, docs){
		if(e){
			res.send(e);
		}
		else{
			res.send(docs);
		}
	})
})


/* POST signup user. */
/*http://localhost:3000/signup*/
router.post('/signup',function(req, res){
	var db = req.db;
	var collection = db.get('usercollection');
	var firstname = req.body.firstname;
	var lastname = req.body.lastname;
	var username = req.body.username;
	var userEmail = req.body.useremail;
	var password = req.body.password;
	var type = req.body.type;
	var createdon = req.body.createdon;
	var rating = req.body.rating;
	var image = req.body.image;
	if(type == "Teacher"){
		var courses = []  
	}
	// var latitude = req.body.latitude;
	// var longitude = req.body.longitude;
	collection.insert({
		"firstname" : firstname, 
		"lastname" : lastname, 
		"username" : username, 
		"email" : userEmail, 
		"password" : password,
		"type" : type,
		"createdon" : createdon,
		"rating" : rating,
		"image" : image,
		"courses" : courses,
		// "latitude"  :latitude,  
		// "longitude" :longitude
	},function(e,docs){
		if (e) {
			// If it failed, return error
			res.send("There was a problem adding the information to the database.");
		}
		else {
			// And forward to success page
			docs.status = 1;
			docs.message = "Signup successfully"
			res.send(docs)
		}
	});
})

/* POST search user. */
/*http://localhost:3000/search*/
router.post('/search',function(req, res){
	var db = req.db;
	var collection = db.get('usercollection');
	var type = req.body.type;
	collection.find({"type" : type},function(e,docs){
		res.send(docs);  
	});
});


router.post('/loadimage', function(req, res){
	res.send(req.body)
})

/* POST send request. */
/*http://localhost:3000/sendrequest*/
router.post('/sendrequest',function(req, res){
	var db = req.db;
	var collection = db.get('requestcollection');
	var requestedByName = req.body.requestedByName;
	var requestedById = req.body.requestedById;
	var requestedForName = req.body.requestedForName;
	var requestedForId = req.body.requestedForId;
	var response = req.body.response;
	collection.insert({
		"requestedByName" : requestedByName,
		"requestedById" : requestedById, 
		"requestedForName" : requestedForName,
		"requestedForId" : requestedForId,
		"response" : response
	},function(e,docs){
		
		res.send(docs);  
	});
});

/* POST get request. */
/*http://localhost:3000/getallrequest*/
router.post('/getallrequest',function(req, res){
	var db = req.db;
	var collection = db.get('requestcollection');
	var requestedForId = req.body.requestedForId;
	collection.find({requestedForId : requestedForId},function(e,docs){
		if(e){
			res.send(e)
		}
		else{
			res.send(docs);  
		}
			
	});
});

/* POST get specific request. */
/*http://localhost:3000/getspecificrequest*/
router.post('/getspecificrequest',function(req, res){
	var db = req.db;
	var collection = db.get('requestcollection');
	var requestedById = req.body.requestedById;
	var requestedForId = req.body.requestedForId;
	console.log(req.body)
	collection.find({requestedById : requestedById, requestedForId : requestedForId},function(e,docs){
		if(e){
			res.send(e)
		}
		else{
			res.send(docs);  
		}
			
	});
});

/* POST send request. */
/*http://localhost:3000/updaterequest*/
router.post('/updaterequest',function(req, res){
	var db = req.db;
	var collection = db.get('requestcollection');
	var id = req.body.id
	var requestedByName = req.body.requestedByName;
	var requestedById = req.body.requestedById;
	var requestedForName = req.body.requestedForName;
	var requestedForId = req.body.requestedForId;
	var response = req.body.response;
	collection.update({"_id" : ObjectId(id)},{requestedById : requestedById, requestedByName : requestedByName, requestedForName : requestedForName, requestedForId : requestedForId, response : response},function(e,docs){
		if(e){
			res.send(e)
		}
		else{
			res.send(docs);  
		}
			
	});
});

router.post('/updateProfile',function(req, res){
	var db = req.db;
	var collection = db.get('usercollection');
	if(req.body.courses){
		var courses = req.body.courses;
	}
	var id = req.body._id
	var firstname = req.body.firstname;
	var lastname = req.body.lastname;
	var email = req.body.email;
	var username = req.body.username
	var password = req.body.password;
	var type = req.body.type;
	var createdon = req.body.createdon;
	var rating = req.body.rating;
	var image = req.body.image;
	var address = req.body.address;
	collection.update({"_id" : ObjectId(id)},{firstname : firstname, lastname : lastname, email : email, password : password, type : type, createdon : createdon, rating : rating, image : image, address : address, courses : courses, username : username},function(e,docs){
		if(e){
			res.send(e)
		}
		else{
			res.send(docs);
		}
			
	});
});

router.post('/updateProfileByAdmin',function(req, res){
	var id = req.body._id
	var db = req.db;
	var collection = db.get('usercollection');
	collection.find({"_id" : ObjectId(id)},function(e, docs){
		if(e){
			res.send(e)
		}
		else{
			docs[0].firstname = req.body.firstname
			docs[0].lastname = req.body.lastname
			docs[0].username = req.body.username
			docs[0].email = req.body.email
			docs[0].password = req.body.password
			docs[0].image = docs[0].image
			docs[0].address = req.body.address
			docs[0].type= req.body.type
			docs[0].createdon = docs[0].createdon
			docs[0].rating = docs[0].rating
			collection.update({"_id" : ObjectId(id)},{firstname : docs[0].firstname, lastname : docs[0].lastname, email : docs[0].email, password : docs[0].password, type : docs[0].type, createdon : docs[0].createdon, rating : docs[0].rating, image : docs[0].image, address : docs[0].address, courses : docs[0].courses, username : docs[0].username},function(e,doc){
				if(e){
					res.send(e)
				}
				else{
					res.send(doc);
				}
			});
		}
	})
});


/* POST get courses . */
/*http://localhost:3000/courses*/
router.post('/courses',function(req, res){
	var db = req.db;
	var collection = db.get('teachercollection');
	var id = req.body.id;
	collection.find({"id" : id},function(e,docs){
		if(e){
			res.send(e)
		}
		else {
			res.send(docs[0].rating);
		}  
	});
});


/* POST get all rating . */
/*http://localhost:3000/allrating*/
router.post('/allrating',function(req, res){
	var db = req.db;
	var collection = db.get('teachercollection');
	var teacherId = req.body.teacherId;
	collection.find({"id" : teacherId},function(e,docs){
		if(e){
			res.send(e)
		}
		else {
			res.send(docs[0]);
		}  
	});
})

/* POST get rating. */
/*http://localhost:3000/getrating*/
router.post('/getrating',function(req, res){
	var db = req.db;
	var collection = db.get('teachercollection');
	var collection1 = db.get('studentcollection');
	var Id = req.body.Id;
	var type = req.body.type;
	if(type == 'Teacher'){
		collection.find({"id" : Id},function(e,docs){
			if(e){
				res.send(e)
			}
			else {
				res.send(docs[0]);
			}  
		});
	}
	else{
		collection1.find({"id" : Id},function(e,docs){
			if(e){
				res.send(e)
			}
			else {
				res.send(docs[0]);
			}  
		});  
	}
	
})  
	
/* POST add courses. */
/*http://localhost:3000/addcourse*/
router.post('/addcourse',function(req, res){
	var db = req.db;
	var collection = db.get('usercollection');
	var collectionOne = db.get('teachercollection');
	var userId = req.body.userId;
	var courseName = req.body.courseName;
	collection.update({"_id" : userId},{ $push: { courses: courseName } }, function(e,docs){
		if(e){
			console.log(e)
			res.send(e)
		}
		else {
			console.log(docs)
			collectionOne.update({"id" : userId},{$push: { rating : {subject: courseName, rate:[]}}},function(e, docs){
				if(e){
					console.log(e)
					res.send(e)
				}
				else{
					console.log(docs)
					res.send(docs);
				}
			})
		}  
	});
});


/*POST update rating*/
/*http://localhost:3000/setTeacherRating*/
router.post('/setTeacherRating', function(req, res){
	var db = req.db;
	var collection = db.get('teachercollection');
	var teacherId = req.body.teacherId;
	var teacherName = req.body.teacherName;
	var studentId = req.body.studentId;
	var studentImage = req.body.studentImage;
	var studentName = req.body.studentName;
	var subject = req.body.subject;
	var rating = req.body.rating - 1;
	var rateing = []
	for(var i=0;i<5;i++){
		if(i<=rating){
			rateing[i]=1
		}
		else{
			rateing[i]=0
		}
	}
	var data = {
		"teacher" : teacherName,
		"id" : teacherId,
		"rating" : [ 
			{
				"subject" : subject,
				"rate" : [ 
					{
						"student" : studentName,
						"id" : studentId,
						"image" : studentImage,
						"rate" : rateing
					}
				]
			}
		]
	}

	var subdata = {
		"subject" : subject,
		"rate" : [ 
			{
				"student" : studentName,
				"id" : studentId,
				"image" : studentImage,
				"rate" : rateing
			}
		]
	} 
	collection.find({"id" : teacherId},function(e,docs){
		if(e){
			res.send(e)
			
		}
		else {
			if(docs.length){
				if(docs[0].rating[0].subject == subject){
					collection.update({"id" : teacherId, "rating.subject" : subject},{$push: { 'rating.$.rate' : {student : studentName, image : studentImage, id : studentId, rate : rateing}}},function(e, docs){
						if(e){
							res.send(e)
						}
						else{
							res.send(docs);
						}
					})
				}
				else{
					collection.update({"id" : teacherId},{$push: { 'rating' : subdata}},function(e, docs){
						if(e){
							res.send(e)
						}
						else{
							res.send(docs);
						}
					})
				}
			}
			else{
				collection.insert(data,function(e,docs){
					if(e){
						res.send(e)
					}
					else{
						res.send(docs)
					}
				})
			}
		}  
	});
})

router.post('/setNewTeacherRating', function(req, res){
	var db = req.db;
	var collection = db.get('teachercollection');
	var teacherId = req.body.teacherId;
	var teacherName = req.body.teacherName;
	var data = {
		"teacher" : teacherName,
		"id" : teacherId,
		"rating" : []
	}
 
	collection.insert(data,function(e,docs){
		if(e){
			res.send(e)
		}
		else {
			res.send(docs);
		}  
	});
})

/*POST update rating*/
/*http://localhost:3000/setStudentRating*/
router.post('/setStudentRating', function(req, res){
	var db = req.db;
	var collection = db.get('studentcollection');
	var teacherId = req.body.teacherId;
	var teacherName = req.body.teacherName;
	var studentId = req.body.studentId;
	var studentName = req.body.studentName;
	var rating = req.body.rating - 1;
	var rateing = []
	for(var i=0;i<5;i++){
		if(i<=rating){
			rateing[i]=1
		}
		else{
			rateing[i]=0
		}
	}
	var data = {
		"Student" : studentName,
		"id" : studentId,
		"rating" : [ 
			{
				"rate" : [ 
					{
						"Teacher" : teacherName,
						"id" : teacherId,
						"rate" : rateing
					}
				]
			}
		]
	} 
	collection.find({"id" : studentId},function(e,docs){
		if(e){
			res.send(e)
		}
		else {
			if(docs.length){
				collection.update({"id" : studentId},{$push: { 'rating.0.rate' : {Teacher : teacherName, id : teacherId, rate : rateing}}},function(e, docs){
					if(e){
						res.send(e)
					}
					else{
						res.send(docs);
					}
				})
			}
			else{
				collection.insert(data,function(e,docs){
					if(e){
						res.send(e)
					}
					else{
						res.send(docs)
					}
				})
			}
		}  
	});
})

/* POST update student main rating. */
/*http://localhost:3000/mainRating*/
router.post('/mainRating',function(req, res){
	var db = req.db;
	var collection = db.get('usercollection');
	var userId = req.body.userId;
	var rate = req.body.rating-1;
	var firstname = req.body.firstname;
	var lastname = req.body.lastname;
	var username = req.body.username;
	var email = req.body.email;
	var password = req.body.password;
	var type = req.body.type;
	var createdon = req.body.createdon;
	var image = req.body.image;
	var address = req.body.address;
	var rating = req.body.rating;  
	var rating = []
	for(var i=0;i<5;i++){
		if(i<=rate){
			rating[i]=1
		}
		else{
			rating[i]=0
		}
	}
	if(type == 'Teacher'){
		var courses = req.body.courses;
		collection.update({"_id" : userId},{
			firstname : firstname, 
			lastname : lastname, 
			username : username, 
			email : email, 
			password : password,
			image : image,
			address : address,
			type : type,
			createdon : createdon,
			rating : rating, 
			courses : courses
		}, function(e,docs){
			if(e){
				res.send(e)
			}
			else {
				res.send(docs);
			}  
		});
	}
	else{
		collection.update({"_id" : userId},{
			firstname : firstname, 
			lastname : lastname, 
			username : username, 
			email : email, 
			password : password, 
			image : image, 
			address : address, 
			type : type, 
			createdon : createdon, 
			rating : rating
		},function(e,docs){
			if(e){
				res.send(e)
			}
			else {
				res.send(docs);
			}  
		});
	}
});

router.post('/upload', upload.any(), function(req, res) {
	var file = req.files
	res.send(file[0].path)
});

router.post('/remove', function(req, res) {
	var db = req.db;
	var collection = db.get('usercollection');
	var id = req.body.user._id
	// console.log(req.body.user._id)
	collection.remove({"_id" : ObjectId(id)}, function(e, docs){
		if(e){
			console.log("error "+e)
			res.send(e)
		}
		else{
			console.log("docs "+docs)
			res.send(docs)
		}
	})
});



module.exports = router;
