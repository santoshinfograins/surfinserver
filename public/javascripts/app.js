var surfin = angular.module('surfin',['ui.router']);

surfin.config(function($stateProvider, $urlRouterProvider) {
	
  $urlRouterProvider.otherwise('/');
	
  $stateProvider
	  
	.state('default', {
	  abstract: true,
	  views: {
			layout: {
			  templateUrl: 'views/layout/default.html',
			}
	  }
	})
	  // login ========================================
	  .state('login', {
			url: '/',
			templateUrl: 'views/login.html',
			controller: 'appController',
			cache: false,
			parent: 'default',
	  })

	.state('sidemenu', {
	  abstract: true,
	  views: {
			layout: {
			  templateUrl: 'views/layout/sidemenu.html',
			}
	  }
	})
		// dashboard ========================================
	  .state('dashboard', {
			url: '/dashboard',
			templateUrl: 'views/dashboard.html',
			controller: 'appController',
			parent: 'sidemenu',
	  })
	  // teacherlist ========================================
	  .state('teacherlist', {
			url: '/teacherlist',
			templateUrl: 'views/teacherlist.html',
			controller: 'appController',
			parent: 'sidemenu',
	  }) 
	  // studentlist ========================================
	  .state('studentlist', {
			url: '/studentlist',
			templateUrl: 'views/studentlist.html',
			controller: 'appController',
			parent: 'sidemenu',
	  })
	  // teacherInfo ========================================
	  .state('teacherInfo', {
			url: '/teacherInfo',
			templateUrl: 'views/teacherInfo.html',
			controller: 'appController',
			parent: 'sidemenu',
	  })
	  // studentInfo ========================================
	  .state('studentInfo', {
			url: '/studentInfo',
			templateUrl: 'views/studentInfo.html',
			controller: 'appController',
			parent: 'sidemenu',
	  }) 
	  // maps ========================================
	  .state('maps', {
			url: '/maps',
			templateUrl: 'views/maps.html',
			controller: 'appController',
			parent: 'sidemenu',
	  })
	  // maps ========================================
	  .state('addUser', {
			url: '/addUser',
			templateUrl: 'views/addUser.html',
			controller: 'appController',
			parent: 'sidemenu',
	  })    
})

// surfin.constant('URL', {
//   // baseUrl:'https://surfinserver.herokuapp.com',
//   baseUrl:'http://192.168.1.110:3000',
// });

surfin.controller('appController', function($rootScope, $scope, $http, $state) {
	$scope.admin = {};
  if($rootScope.users == undefined || $rootScope.users == null || $rootScope.users == ''){
  	$rootScope.users = []
  }
  if($rootScope.teachers == '' || $rootScope.teachers == undefined){
  	$rootScope.teachers = [];	
  }
  if($rootScope.students == '' || $rootScope.students == undefined){
  	$rootScope.students = [];	
  }
  
 	$scope.getUsers = function(){
  	$rootScope.users = []
		$rootScope.teachers = []
		$rootScope.students = []  	
  	$http({
  		method : 'GET',
  		url : '/getusers',
  	})
  	.success(function(success){

  		$rootScope.users = success;
  		var rateStudent = 0;
  		var rateTeacher = 0;
  		for(var i in $scope.users){
				if($scope.users[i].type == 'Teacher'){
					for(var j in $scope.users[i].rating){
						if($scope.users[i].rating[j]){
							rateTeacher = rateTeacher + 1;
						}
					}
					$rootScope.users[i].rating = rateTeacher
					rateTeacher = 0;
					$rootScope.teachers.push($rootScope.users[i])
				}
				else{
					for(var j in $scope.users[i].rating){
						if($scope.users[i].rating[j]){
							rateStudent = rateStudent + 1;
						}
					}
					$rootScope.users[i].rating = rateStudent
					rateStudent = 0;
					$rootScope.students.push($rootScope.users[i])
				}
			}
  	})
  	.error(function(error){
  		console.log(error)
  	})
  }

 	$scope.login = function(){
  	console.log($scope.admin.password )
    if($scope.admin.email != 'adminsurfin@yopmail.com'){
      alert('Please login as Admin')
    }
    else{
      var data = { email : $scope.admin.email, password : $scope.admin.password }
      $http({
        method : 'POST',
        url : '/login',
        data : data
      })
      .success(function(success){
        $rootScope.users = success;
        var rateStudent = 0;
        var rateTeacher = 0;
        for(var i in $scope.users){
          if($scope.users[i].type == 'Teacher'){
            for(var j in $scope.users[i].rating){
              if($scope.users[i].rating[j]){
                rateTeacher = rateTeacher + 1;
              }
            }
            $rootScope.users[i].rating = rateTeacher
            rateTeacher = 0;
            $rootScope.teachers.push($rootScope.users[i])
          }
          else{
            for(var j in $scope.users[i].rating){
              if($scope.users[i].rating[j]){
                rateStudent = rateStudent + 1;
              }
            }
            $rootScope.users[i].rating = rateStudent
            rateStudent = 0;
            $rootScope.students.push($rootScope.users[i])
          }
        }
        $state.go('dashboard')
      })
      .error(function(error){
        console.log(error)
      })
    }
  }

  $scope.teacherDetail = function(teacher){
  	if($state.current.name == 'teacherlist' || $state.current.name == 'studentlist'){
  		$rootScope.test = true
  	}
  	else{
  		$rootScope.test = false
  	}
  	$rootScope.selectedTeacher = teacher
  	$http({
  		method : 'POST',
  		url : '/getrating',
  		data : { Id : $rootScope.selectedTeacher._id, type : $rootScope.selectedTeacher.type}
  	})
  	.success(function(success){
  		$rootScope.selectedTeacherDeatil = success
  		$state.go('teacherInfo')
  	})
  	.error(function(error){
  		console.log(error)
  	})
  }

  $scope.studentDetail = function(student){
  	if($state.current.name == 'teacherlist' || $state.current.name == 'studentlist'){
  		$rootScope.test = true
  	}
  	else{
  		$rootScope.test = false
  	}
  	$rootScope.selectedStudent = student
  	var rateStudent = 0
  	$http({
  		method : 'POST',
  		url : '/getrating',
  		data : { Id : $rootScope.selectedStudent._id, type : $rootScope.selectedStudent.type}
  	})
  	.success(function(success){
  		$rootScope.selectedStudentDeatil = success
  		// for(var i in $rootScope.selectedStudentDeatil.rating){
  			
  		// 	for(var j in $rootScope.selectedStudentDeatil.rating[i].rate){
  				
  		// 		for(var k in $rootScope.selectedStudentDeatil.rating[i].rate){
  					
  		// 			for(var l in $rootScope.selectedStudentDeatil.rating[i].rate[k].rate){
	  	// 				console.log($rootScope.selectedStudentDeatil.rating[i].rate[k].rate[l])
	  	// 				if($rootScope.selectedStudentDeatil.rating[i].rate[k].rate[l]){
	  	// 					rateStudent = rateStudent + 1
	  	// 				}

	  	// 			}
	  	// 			$rootScope.selectedStudentDeatil.rating[i].rate[k].rate = rateStudent
	  	// 			// console.log($rootScope.selectedStudentDeatil.rating[i].rate[k].rate)
	  				
  		// 		}
  		// 		rateStudent = 0;
  		// 	}

  		// }
  		$state.go('studentInfo')
  	})
  	.error(function(error){
  		console.log(error)
  	})
  }

  

  $scope.removeUser = function(user){
  	BootstrapDialog.show({
      title: 'Delete User',
      message: 'Are you sure, delete '+user.username,
      buttons: [
	      {
	        label: 'Ok',
	        action: function(dialog) {
	          $http({
	          	method : 'POST',
	          	url : '/remove',
	          	data : {user : user}
	          })
	          .success(function(success){
	          	$scope.getUsers()
	          	dialog.close();
	          })
	          .error(function(error){
	          	console.log(error)
	          })	
	        }
	      }, 
	      {
	        label: 'Cancle',
	        action: function(dialog) {
	            dialog.close();
	        }
	      }
      ]
  	});
  }

  $scope.editUser = function(user){
  	if(user.type == 'Teacher'){
  		$scope.teacherDetail(user);
  	}
  	else{
  		$scope.studentDetail(user);
  	}
  }

  $scope.updateStudent = function(){
  	$http({
  		method : 'POST',
  		url : '/updateProfileByAdmin',
  		data : $rootScope.selectedStudent
  	})
  	.success(function(success){
  		console.log(success)
  		$scope.getUsers();
  		$state.go('dashboard')
  	})
  	.error(function(error){
  		console.log(error)
  	})
  }

  $scope.updateTeacher = function(){
  	$http({
  		method : 'POST',
  		url : '/updateProfileByAdmin',
  		data : $rootScope.selectedTeacher
  	})
  	.success(function(success){
  		console.log(success)
  		$scope.getUsers();
  		$state.go('dashboard')
  	})
  	.error(function(error){
  		console.log(error)
  	})
  }

  // if($scope.addUserDisabled == undefined){
  // 	$scope.addUserDisabled = false;	
  // }
  
  
  if($state.current.url == '/dashboard'){
  	$scope.addUserDisabled = true;
  }

  $scope.adduser = function(){
  	console.log($scope.newUser)
  	$http({
  		method : 'POST',
  		url : 'signup',
  		data : $scope.newUser
  	})
  	.success(function(success){
  		console.log(success)
  		$scope.getUsers()
  		$state.go('dashboard')
  	})
  	.error(function(error){
  		console.log(error)
  	})
  }

});

surfin.directive('fallbackSrc', function () {
  var fallbackSrc = {
    link: function postLink(scope, iElement, iAttrs) {
      iElement.bind('error', function() {
        angular.element(this).attr("src", iAttrs.fallbackSrc);
      });
    }
  }
  return fallbackSrc;
});